package com.example.lab2;

import javax.swing.*;
import java.awt.*;

public class Scene extends JPanel {
    protected Graphics2D graphics2D;

    public void paint(Graphics g) {
        super.paint(g);
        graphics2D = (Graphics2D) g;
        setUpRenderingSettings(graphics2D);
    }

    protected void setUpRenderingSettings(Graphics2D graphics2D) {
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);
    }
}
