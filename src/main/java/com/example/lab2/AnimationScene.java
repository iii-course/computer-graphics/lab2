package com.example.lab2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AnimationScene extends Scene implements ActionListener {
    private Timer timer;

    private static int maxWidth;
    private static int maxHeight;

    private static double animationFrameSize = 0;

    private boolean isHorizontalDirection = true;
    private int speed = 1;

    private static double leftOffset = 0;
    private static double topOffset = 0;

    private static double minLeftOffset = 0;
    private static double maxLeftOffset = 0;

    private static double minTopOffset = 0;
    private static double maxTopOffset = 0;

    private static FramePainter framePainter;
    private static ButterflyPainter butterflyPainter;

    public AnimationScene() {
        timer = new Timer(10, this);
        timer.start();
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Lab2");

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1080, 720);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.add(new AnimationScene());
        frame.setVisible(true);

        Dimension size = frame.getSize();
        Insets insets = frame.getInsets();
        maxWidth = size.width - insets.left - insets.right - 1;
        maxHeight = size.height - insets.top - insets.bottom - 1;

        setUpDimensions();

        framePainter = new FramePainter(maxWidth, maxHeight);
        butterflyPainter = new ButterflyPainter();
    }

    public void paint(Graphics g) {
        super.paint(g);
        
        final Graphics2D graphics2D = (Graphics2D) g;

        framePainter.setGraphics2D(graphics2D);
        framePainter.paint();

        butterflyPainter.setGraphics2D(graphics2D);
        butterflyPainter.paint();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (maxLeftOffset == 0 || maxTopOffset == 0) {
            return;
        }
        if (isHorizontalDirection) {
            leftOffset += speed;
            butterflyPainter.increaseSize();
            if (leftOffset >= maxLeftOffset) {
                leftOffset = maxLeftOffset;
                isHorizontalDirection = false;
            } else if (leftOffset <= minLeftOffset) {
                leftOffset = minLeftOffset;
                this.isHorizontalDirection = false;
            }
        } else {
            topOffset += speed;
            butterflyPainter.decreaseSize();
            if (topOffset >= maxTopOffset) {
                topOffset = maxTopOffset;
                speed = -speed;
                this.isHorizontalDirection = true;
            }
            else if (topOffset <= minTopOffset) {
                topOffset = minTopOffset;
                speed = -speed;
                this.isHorizontalDirection = true;
            }
        }
        butterflyPainter.setCentreX(leftOffset);
        butterflyPainter.setCentreY(topOffset);
        repaint();
    }

    private static void setUpDimensions() {
        animationFrameSize = Math.min(maxWidth, maxHeight) * 0.3;

        final int headerHeight = 30;
        leftOffset = maxWidth / 2 - animationFrameSize / 2;
        topOffset = maxHeight / 2 - animationFrameSize / 2 - headerHeight;

        minLeftOffset = leftOffset;
        minTopOffset = topOffset;

        maxLeftOffset = leftOffset + animationFrameSize;
        maxTopOffset = topOffset + animationFrameSize;
    }
}
