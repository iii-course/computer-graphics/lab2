package com.example.lab2;

import java.awt.*;

public class FramePainter implements Painter {
    private final int strokeWidth = 10;

    private Graphics2D graphics2D;
    private final int maxWidth;
    private final int maxHeight;

    private final int staticFrameMargin = 50;

    public FramePainter(int maxWidth, int maxHeight) {
        this.maxWidth = maxWidth;
        this.maxHeight = maxHeight;
    }

    public void setGraphics2D(Graphics2D graphics2D) {
        this.graphics2D = graphics2D;
    }

    @Override
    public void paint() {
        // palette: https://coolors.co/palette/f72585-b5179e-7209b7-560bad-480ca8-3a0ca3-3f37c9-4361ee-4895ef-4cc9f0
        graphics2D.setColor(new Color(76, 201, 240));

        final int width = maxWidth - staticFrameMargin * 2;
        final int height = maxHeight - staticFrameMargin * 2;

        BasicStroke stroke = new BasicStroke(strokeWidth, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL);
        graphics2D.setStroke(stroke);

        graphics2D.drawRect(staticFrameMargin, staticFrameMargin, width, height);
    }
}
