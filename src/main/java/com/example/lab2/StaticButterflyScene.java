package com.example.lab2;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.Color;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;

public class StaticButterflyScene implements Painter {
    private final int whiskerDepth = 15;
    private final int whiskerHeight = 100;
    private final int whiskerBase = 10;

    private int centreX;
    private int centreY;

    private Graphics2D graphics2D;

    public StaticButterflyScene(Graphics2D graphics2D, int maxWidth, int maxHeight) {
        this.graphics2D = graphics2D;
        centreX = maxWidth / 2;
        centreY = maxHeight / 2;
    }

    @Override
    public void paint() {
        int bodyRadiusX = 20;
        int bodyRadiusY = 80;

        drawBody(bodyRadiusX, bodyRadiusY);
        drawWhiskers(bodyRadiusX, bodyRadiusY);
        drawWings();
    }

    private void drawBody(int bodyRadiusX, int bodyRadiusY) {
        Ellipse2D body = new Ellipse2D.Double(
                centreX - bodyRadiusX,
                centreY - bodyRadiusY,
                bodyRadiusX * 2,
                bodyRadiusY * 2
        );
        graphics2D.setColor(Color.GREEN);
        graphics2D.fill(body);
    }

    private void drawWhiskers(int bodyRadiusX, int bodyRadiusY) {
        Line2D whiskerLeft = new Line2D.Double(
                centreX - bodyRadiusX + whiskerDepth,
                centreY - bodyRadiusY + whiskerDepth,
                centreX - bodyRadiusX + whiskerDepth - whiskerBase,
                centreY - bodyRadiusY + whiskerDepth - whiskerHeight
        );

        Line2D whiskerRight = new Line2D.Double(
                centreX + bodyRadiusX - whiskerDepth,
                centreY - bodyRadiusY + whiskerDepth,
                centreX + bodyRadiusX - whiskerDepth + whiskerBase,
                centreY - bodyRadiusY + whiskerDepth - whiskerHeight
        );

        graphics2D.setColor(Color.BLACK);
        graphics2D.draw(whiskerLeft);
        graphics2D.draw(whiskerRight);
    }

    private void drawWings() {
        graphics2D.setColor(Color.CYAN);

        GeneralPath wingLeftTop = new GeneralPath();
        wingLeftTop.moveTo(centreX, centreY);
        wingLeftTop.lineTo(centreX - 100, centreY - 140);
        wingLeftTop.lineTo( centreX - 200, centreY - 30);
        wingLeftTop.closePath();

        graphics2D.fill(wingLeftTop);

        GeneralPath wingRightTop = new GeneralPath();
        wingRightTop.moveTo(centreX, centreY);
        wingRightTop.lineTo( centreX + 100, centreY - 140);
        wingRightTop.lineTo( centreX + 200, centreY - 30);
        wingRightTop.closePath();

        graphics2D.fill(wingRightTop);

        GeneralPath wingLeftBottom = new GeneralPath();
        wingLeftBottom.moveTo(centreX, centreY);
        wingLeftBottom.lineTo(centreX - 100, centreY + 140);
        wingLeftBottom.lineTo(  centreX - 200, centreY + 30);
        wingLeftBottom.closePath();

        graphics2D.fill(wingLeftBottom);

        GeneralPath wingRightBottom = new GeneralPath();
        wingRightBottom.moveTo(centreX, centreY);
        wingRightBottom.lineTo(centreX + 100, centreY + 140);
        wingRightBottom.lineTo( centreX + 200, centreY + 30);
        wingRightBottom.closePath();

        graphics2D.fill(wingRightBottom);
    }
}
