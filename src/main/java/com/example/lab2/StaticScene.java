package com.example.lab2;

import javax.swing.*;
import java.awt.*;

public class StaticScene extends Scene {
    private static int maxWidth;
    private static int maxHeight;

    public static void main(String[] args) {
        JFrame frame = new JFrame("Lab2");

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1080, 720);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.add(new StaticScene());
        frame.setVisible(true);

        Dimension size = frame.getSize();
        Insets insets = frame.getInsets();
        maxWidth = size.width - insets.left - insets.right - 1;
        maxHeight = size.height - insets.top - insets.bottom - 1;
    }

    public void paint(Graphics g) {
        super.paint(g);

        StaticButterflyScene butterflyScene = new StaticButterflyScene(graphics2D, maxWidth, maxHeight);
        //butterflyScene.paint();

        StaticShapesScene shapesScene = new StaticShapesScene(graphics2D);
         shapesScene.paint();
    }
}