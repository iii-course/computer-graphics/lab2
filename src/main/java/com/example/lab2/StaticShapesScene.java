package com.example.lab2;

import java.awt.*;
import java.awt.geom.Arc2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;

public class StaticShapesScene implements Painter {
    private Graphics2D graphics2D;
    private int leftOffset = 50;
    private final int rightMargin = 50;
    private final int topOffset = 50;

    public StaticShapesScene(Graphics2D graphics2D) {
        this.graphics2D = graphics2D;
    }

    @Override
    public void paint() {
        // palette: https://coolors.co/palette/fbf8cc-fde4cf-ffcfd2-f1c0e8-cfbaf0-a3c4f3-90dbf4-8eecf5-98f5e1-b9fbc0
        GradientPaint gradientPaint = new GradientPaint(
                5, 25, new Color(255, 207, 210),
                20, 2, new Color(253, 228, 207),
                true
        );
        graphics2D.setPaint(gradientPaint);
        drawEllipse(250, 250);

        graphics2D.setColor(new Color(207, 186, 240));
        drawArc(100, 300);

        graphics2D.setColor(new Color(144, 219, 244));
        drawRectangle(150, 250);

        graphics2D.setColor(new Color(185, 251, 192));
        drawStar(160, 80, 6);
    }

    private void drawEllipse(int width, int height) {
        Ellipse2D ellipse = new Ellipse2D.Double(leftOffset, topOffset, width, height);
        leftOffset += width + rightMargin;
        graphics2D.fill(ellipse);
    }

    private void drawArc(int width, int height) {
        Arc2D arc = new Arc2D.Double(leftOffset, topOffset, width, height, 90, 270, Arc2D.PIE);
        leftOffset += width + rightMargin;
        graphics2D.fill(arc);
    }

    private void drawRectangle(int width, int height) {
        Rectangle2D rectangle = new Rectangle2D.Double(leftOffset, topOffset, width, height);
        leftOffset += width + rightMargin;
        graphics2D.fill(rectangle);
    }

    private void drawStar(int peakLength, int peakDifference, int numberOfPeaks) {
        final int angleStep = 360 / numberOfPeaks / 2;

        boolean isPeak = true;
        int centreX = leftOffset + peakLength;
        int centreY = topOffset + peakLength;

        GeneralPath star = new GeneralPath();

        for (int currentAngle = 0; currentAngle < 360; currentAngle += angleStep) {
            int bodyRadius = isPeak ? peakLength : peakLength - peakDifference;
            double nextPointX = centreX + bodyRadius * Math.cos(Math.toRadians(currentAngle));
            double nextPointY = centreY + bodyRadius * Math.sin(Math.toRadians(currentAngle));
            if (currentAngle == 0) {
                star.moveTo(nextPointX, nextPointY);
            } else {
                star.lineTo(nextPointX, nextPointY);
            }
            isPeak = !isPeak;
        }

        star.closePath();
        graphics2D.fill(star);
    }
}
