package com.example.lab2;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.Color;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;

public class ButterflyPainter implements Painter {
    private Graphics2D graphics2D;

    private final double minBodyRadiusX = 5;
    private final double maxBodyRadiusX = 15;
    private final double dx = 0.02;
    private double bodyRadiusX = 10;

    private double centreX;
    private double centreY;

    public void setGraphics2D(Graphics2D graphics2D) {
        this.graphics2D = graphics2D;
    }

    @Override
    public void paint() {
        drawBody();
        drawWhiskers();
        drawWings();
    }

    public void setCentreX(double x) {
        centreX = x;
    }

    public void setCentreY(double y) {
        centreY = y;
    }

    public void increaseSize() {
        bodyRadiusX += dx;
        if (bodyRadiusX > maxBodyRadiusX) {
            bodyRadiusX = maxBodyRadiusX;
        }
    }

    public void decreaseSize() {
        bodyRadiusX -= dx;
        if (bodyRadiusX < minBodyRadiusX) {
            bodyRadiusX = minBodyRadiusX;
        }
    }

    private double getBodyRadiusY() {
        return bodyRadiusX * 4;
    }

    private double getWhiskerDepth() {
        return bodyRadiusX;
    }

    private double getWhiskerBase() {
        return bodyRadiusX / 2;
    }

    private double getWhiskerHeight() {
        return bodyRadiusX * 5;
    }

    private double getWingsSize() {
        return getBodyRadiusY() / 0.8;
    }

    private void drawBody() {
        final double bodyRadiusY = getBodyRadiusY();

        Ellipse2D body = new Ellipse2D.Double(
                centreX - bodyRadiusX,
                centreY - bodyRadiusY,
                bodyRadiusX * 2,
                bodyRadiusY * 2
        );
        graphics2D.setColor(Color.GREEN);
        graphics2D.fill(body);
    }

    private void drawWhiskers() {
        final double bodyRadiusY = getBodyRadiusY();
        final double whiskerDepth = getWhiskerDepth();
        final double whiskerBase = getWhiskerBase();
        final double whiskerHeight = getWhiskerHeight(); 

        Line2D whiskerLeft = new Line2D.Double(
                centreX - bodyRadiusX + whiskerDepth,
                centreY - bodyRadiusY + whiskerDepth,
                centreX - bodyRadiusX + whiskerDepth - whiskerBase,
                centreY - bodyRadiusY + whiskerDepth - whiskerHeight
        );

        Line2D whiskerRight = new Line2D.Double(
                centreX + bodyRadiusX - whiskerDepth,
                centreY - bodyRadiusY + whiskerDepth,
                centreX + bodyRadiusX - whiskerDepth + whiskerBase,
                centreY - bodyRadiusY + whiskerDepth - whiskerHeight
        );

        BasicStroke stroke = new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL);
        graphics2D.setStroke(stroke);
        graphics2D.setColor(Color.BLACK);
        graphics2D.draw(whiskerLeft);
        graphics2D.draw(whiskerRight);
    }

    private void drawWings() {
        graphics2D.setColor(Color.CYAN);

        final double size = getWingsSize();

        GeneralPath wingLeftTop = new GeneralPath();
        wingLeftTop.moveTo(centreX, centreY);
        wingLeftTop.lineTo(centreX - size, centreY - 1.4 * size);
        wingLeftTop.lineTo( centreX - 2 * size, centreY - 0.3 * size);
        wingLeftTop.closePath();

        graphics2D.fill(wingLeftTop);

        GeneralPath wingRightTop = new GeneralPath();
        wingRightTop.moveTo(centreX, centreY);
        wingRightTop.lineTo( centreX + size, centreY - 1.4 * size);
        wingRightTop.lineTo( centreX + 2 * size, centreY - 0.3 * size);
        wingRightTop.closePath();

        graphics2D.fill(wingRightTop);

        GeneralPath wingLeftBottom = new GeneralPath();
        wingLeftBottom.moveTo(centreX, centreY);
        wingLeftBottom.lineTo(centreX - size, centreY + 1.4 * size);
        wingLeftBottom.lineTo(  centreX - 2 * size, centreY + 0.3 * size);
        wingLeftBottom.closePath();

        graphics2D.fill(wingLeftBottom);

        GeneralPath wingRightBottom = new GeneralPath();
        wingRightBottom.moveTo(centreX, centreY);
        wingRightBottom.lineTo(centreX + size, centreY + 1.4 * size);
        wingRightBottom.lineTo( centreX + 2 * size, centreY + 0.3 * size);
        wingRightBottom.closePath();

        graphics2D.fill(wingRightBottom);
    }
}
